import React, {useEffect, useState} from 'react';
import {Header} from "./components/header";
import {FirstSection} from "./components/sections/first";
import {SecondSection} from "./components/sections/second";
import {ResponsiveHeader} from "./components/header/responsive";

function App() {
  let [width, setWidth] = useState(window.innerWidth)
  useEffect(() => {
    function handleResize(e) {
      setWidth(e.currentTarget.innerWidth);
    }
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, [width])
  return (
    <React.Fragment>
      {
        width > 600
        ? <Header/>
        : <ResponsiveHeader/>
      }
      <FirstSection/>
      <SecondSection/>
    </React.Fragment>
  );
}

export default App;
