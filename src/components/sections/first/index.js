import React from "react";
import styled from "styled-components";
import SkyImage from '../../../images/ian-dooley-DuBNA1QMpPA-unsplash.jpg'
import TakeOffImage from '../../../images/svg/js/takeoff-the-plane.svg'

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  position: relative;
  @media (max-width: 800px) {
    max-height: 100vh;
    height: auto;
    min-height: 420px;
  }
  @media (max-width: 600px) {
    max-height: 100vh;
    height: auto;
    min-height: 400px;
  }
`
const Img = styled.img `
  width: 100vw;
  height: 100vh;
  position: absolute;
  z-index: 0;
  top: 0;
  left: 0;
  @media (max-width: 800px) {
    max-height: 100vh;
    height: auto;
    min-height: 400px;
  }
  @media (max-width: 600px) {
    max-height: 100vh;
    height: auto;
        min-height: 400px;

  }
`
const TextContentContainer = styled.div `
  display: flex;
  flex-direction: column;
  position: absolute;
  z-index: 2;
  top: 16vh;
  left: 2.5vw;
  @media (max-width: 600px) {
    top: 5vh;
  }
`
const TopText = styled.p `
  color: #0e4677;
  font-size: 20px;
  font-family: Montserrat;
  margin-bottom: 2.5px;
  @media (max-width: 650px) {
    margin-left: 0;
    width: 90vw;
  }
`
const TitleText = styled.p `
  color: #fff;
  font-size: 70px;
  font-family: "Roboto Slab";
  width: 55vw;
  @media (max-width: 970px) {
    font-size: 50px;
    width: 70vw;
  }
  @media (max-width: 650px) {
    font-size: 50px;
    width: 90vw;
  }
  @media (max-width: 500px) {
    font-size: 30px;
    width: 90vw;
  }
`
const DescripText = styled.p `
  color: #fff;
  font-size: 18px;
  font-family: Montserrat;
  margin-top: 3.5vh;
  margin-left: 4vw;
  @media (max-width: 650px) {
    margin-left: 0;
    width: 90vw;
  }
`
const TakeOffButton = styled.div `
  margin-left: 4vw;
  font-size: 22px;
  color: #fff;
  text-decoration: underline;
  font-family: Montserrat;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  cursor: pointer;
  margin-top: 2vh;
  &:hover {
    text-decoration: none;
    color: #0e4677;
  }
  @media (max-width: 650px) {
    margin-left: 0;
  }
`
const TakeOffContainerSvg = styled.svg `
  fill: #fff;
  margin-left: 1vw;
  ${TakeOffButton}:hover & {
    fill: #0e4677;
  }
`

const strings = {
  topText: "LAST TAKE OFF: MARCH 19 - 2019",
  title: "Get the first ballon after quarentine. Prepare to take off",
  descripText: "The best experience to perform at the end of the quarantine",
  takeOff: "TAKE OFF"
}

export const FirstSection = () => {

  return(
    <Container>
      <Img src={SkyImage} />
      <TextContentContainer>
        <TopText>{strings.topText}</TopText>
        <TitleText>{strings.title}</TitleText>
        <DescripText>{strings.descripText}</DescripText>
        <TakeOffButton>
          {strings.takeOff}
          <TakeOffContainerSvg
            viewBox="0 0 371.656 371.656"
            width={"2.8vw"}
          >
            <TakeOffImage style={{marginLeft: "1vw"}} fill={"#fff"} width={"3vw"} />
          </TakeOffContainerSvg>
        </TakeOffButton>
      </TextContentContainer>
    </Container>
  )
}
