import React, {useEffect, useState} from 'react'
import styled from "styled-components";
import WhiteSky from '../../../images/white-sky.jpg'
import Emoji from '../../../images/svg/js/emoji.svg'
import {Card} from "../../card";

const Container = styled.div `
  width: 100vw;
  height: 160vh;
  position: relative;
  overflow: hidden;
`
const Img = styled.img `
  width: 100vw;
  height: max-content;
  position: absolute;
  z-index: 0;
  top: 0;
  left: 0;
`
const TextContentContainer = styled.div `
  display: flex;
  flex-direction: column;
  position: absolute;
  z-index: 2;
  top: 8vh;
  left: 5vw;
`
const TopText = styled.p `
  color: #fca0c5;
  font-size: 16px;
  font-family: Montserrat;
  margin-bottom: 2vh;
  margin-left: 1vh;
  display: inline-block;
`
const TitleText = styled.p `
  color: #303338;
  font-size: 70px;
  font-family: "Roboto Slab";
  width: 55vw;
  font-weight: 300;
  display: inline-block;
  align-items: flex-end;
  @media (max-width: 1000px) {
   width: 80vw;
  }
  @media (max-width: 650px) {
   width: 100vw;
  }
  @media (max-width: 450px) {
   font-size: 40px;
  }
`
const DescripText = styled.p `
  color: #303338;
  font-size: 16px;
  font-family: Montserrat;
  margin-top: 3.5vh;
  margin-left: 2vw;
  @media (max-width: 600px) {
    width: 90vw;
    margin-left: 0;
  }
`

const ContainerButton = styled.div `
  padding: 0.5vw 1vw;
  border: 1px solid #303338;
  margin-top: 3.5vh;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 165px;
  height: 30px;
  border-radius: 4px;
  margin-left: 2vw;
  cursor: pointer;
  &:hover {
    border: 3px solid #303338;
  }
  @media (max-width: 450px) {
   margin-left: 0;
  }
`

const CardTitleText = styled.p `
  color: #303338;
  font-size: 19px;
  font-family: Montserrat;
  margin-top: 5vh;
`

const CardsContainer = styled.div `
  width: 88vw;
  padding-right: 12vw;
  display: flex;
  justify-content: space-between;
  margin-top: 4vh;
  @media (max-width: 900px) {
    justify-content: space-around;
  }
`

const strings = {
  title: "Loren Ipsum",
  smallTextInTitle: "By Hot Air Ballon",
  descrip: <React.Fragment>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    <br/>Duis eget feugiat ipsum. Sed a risus sed justo blandit auctor.
    <br/>Proin sodales, sem at feugiat ultrices, velit mi efficitur purus.
  </React.Fragment>,
  secondDescrip: <React.Fragment>
  Lorem ipsum <b>dolor sit amet,</b> consectetur adipiscing elit.
    <br/>Duis eget feugiat ipsum. <b>Sed a risus sed justo blandit auctor.</b>
</React.Fragment>,
  exampleButton: "Example Button",
  exampleCards: "EXAMPLE CARDS"
}

export const SecondSection = () => {
  let [width, setWidth] = useState(window.innerWidth)
  useEffect(() => {
    function handleResize(e) {
      setWidth(e.currentTarget.innerWidth);
    }
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, [width])
  return (
    <Container>
      <Img src={WhiteSky} />
      <TextContentContainer>
        <TitleText>
          {strings.title}
        </TitleText>
        <TopText>{strings.smallTextInTitle}</TopText>
        <DescripText>{strings.descrip}</DescripText>
        <DescripText>{strings.secondDescrip}</DescripText>
        <ContainerButton>
          <DescripText style={{margin: 0}}>{strings.exampleButton}</DescripText>
          <Emoji style={{marginLeft: "1vw"}} width={20} height={20} />
        </ContainerButton>
        <CardTitleText>{strings.exampleCards}</CardTitleText>
        <CardsContainer>
          <Card />
          {
            width > 450 && (
              <Card />
            )
          }
          {
            width > 900 && [
              <Card />,
              <Card />
            ]
          }
        </CardsContainer>
      </TextContentContainer>
    </Container>
  )
}
