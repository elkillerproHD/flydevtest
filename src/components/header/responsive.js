import React, { useState } from 'react'
import styled from "styled-components";
import MenuIcon from '../../images/svg/js/menu.svg'
import LogoImage from "../../images/svg/js/hot-air-ballon.svg";

const CircleClosedFloatButton = styled.div `
  width: 70px;
  height: 70px;
  border-radius: 10vw;
  background-color: #fff;
  position: fixed;
  z-index: 3;
  bottom: 5vw;
  right: 5vw;
  -webkit-box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.75);
  -moz-box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.75);
  box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.75);
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  opacity: 1;
  &:hover {
    opacity: .9;
  }
`

const Container = styled.div `
  width: 100vw;
  height: 30vh;
  background-color: #0e4677;
  position: fixed;
  top: 70vh;
  left: 0;
  z-index: 3;
`
const SubContainer = styled.div `
  width: 100%;
  height: 100%;
  position: relative;
`
const Cross = styled.div `
  color: #fff;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 2vw;
  right: 6vw;
  font-family: Montserrat;
  font-size: 28px;
  cursor: pointer;
  z-index: 4;
`
const LogoContainer = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
  height: 70px;
  -webkit-animation: scale-up-center 1s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
  animation: scale-up-center 1s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
`
const LogoAnimation = styled.div `
  cursor: pointer;
`
const TextLogo = styled.p `
  color: #fff;
  font-size: 30px;
  font-family: Montserrat;
  cursor: pointer;
`

const MenuContainer = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

const MenuButtonStyle = styled.p`
  color: #fff;
  font-size: 18px;
  font-family: Montserrat;
  text-decoration: underline;
  cursor: pointer;
  opacity: 1;
  margin-top: 1vw;
  &:hover {
    text-decoration: none;
    opacity: .9;
  }
  &:active {
    color: #5c90b5;
  }
`

const MenusButton = [
  "ABOUT",
  "TAKE A BALLON",
  "CONTACT"
]

export const ResponsiveHeader = (props) => {
  let [openState, setOpenState] = useState(false)
  if(!openState){
    return (
      <CircleClosedFloatButton onClick={() => setOpenState(true)}>
        <MenuIcon width={45} height={45} fill={"#0e4677"} />
      </CircleClosedFloatButton>
    )
  } else {
    return (
      <Container>
        <SubContainer>
          <Cross onClick={() => setOpenState(false)}>X</Cross>
          <LogoAnimation>
            <LogoContainer>
              <LogoImage height={"50px"} />
              <TextLogo>Air Ballon</TextLogo>
            </LogoContainer>
          </LogoAnimation>
          <MenuContainer>
            {
              MenusButton.map((btn) => {
                return <MenuButtonStyle>{btn}</MenuButtonStyle>
              })
            }
          </MenuContainer>
        </SubContainer>
      </Container>
    )
  }
  return null
}
