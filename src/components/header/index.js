import React from 'react'
import styled from 'styled-components'
import LogoImage from '../../images/svg/js/hot-air-ballon.svg'
import '../../animations/scale-up-center.css'
import '../../animations/rotate-scale-down-ver.css'

const Container = styled.div `
  width: 95vw;
  padding-left: 2.5vw;
  padding-right: 2.5vw;
  height: 70px;
  background-color: #0e4677;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 3;
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const LogoContainer = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
  height: 70px;
  -webkit-animation: scale-up-center 1s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
  animation: scale-up-center 1s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
`
const LogoAnimation = styled.div `
  cursor: pointer;
  &:hover {
    -webkit-animation: rotate-scale-down-ver 1.5s ease-in-out both;
    animation: rotate-scale-down-ver 1.5s ease-in-out both;
  }
`
const TextLogo = styled.p `
  color: #fff;
  font-size: 30px;
  font-family: Montserrat;
  cursor: pointer;
`
const MenuContainer = styled.div `
  height: 70px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 4vw;
`

const MenuButtonStyle = styled.p`
  color: #fff;
  font-size: 18px;
  font-family: Montserrat;
  text-decoration: underline;
  cursor: pointer;
  opacity: 1;
  margin-left: 1vw;
  &:hover {
    text-decoration: none;
    opacity: .9;
  }
  &:active {
    color: #5c90b5;
  }
`

const MenusButton = [
  "ABOUT",
  "TAKE A BALLON",
  "CONTACT"
]

export const Header = () => {
  return (
    <Container>
      <LogoAnimation>
        <LogoContainer>
          <LogoImage height={"50px"} />
          <TextLogo>Air Ballon</TextLogo>
        </LogoContainer>
      </LogoAnimation>
      <MenuContainer>
        {
          MenusButton.map((btn) => {
            return <MenuButtonStyle>{btn}</MenuButtonStyle>
          })
        }
      </MenuContainer>
    </Container>
  )
}
