import React from 'react'
import styled from "styled-components";

const Container = styled.div `
  -webkit-box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.75);
  -moz-box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.75);
  box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.75);
  width: 190px;
  height: 390px;
  background-color: #fff;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`
const TopSubcontainer = styled.div `
  width: 170px;
  height: 245px;
  padding-left:10px;
  padding-right: 10px;
`
const BottomSubcontainer = styled.div `
  width: 170px;
  padding-left: 10px;
  padding-right: 10px;
  height: ${390-245}px;
  background-color: #ffa1c6;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
`
const TopText = styled.p `
  color: #838487;
  font-size: 22px;
  font-family: "Roboto Slab";
  margin-top: 10px;
  font-weight: 700;
`
const TitleText = styled.p `
  color: #525559;
  font-size: 36px;
  font-family: "Roboto Slab";
  margin-top: 10px;
  font-weight: 700;
  margin-bottom: 10px;
`

const NamesText = styled.p`
  color: #838487;
  font-size: 10px;
  font-family: "Montserrat";
  margin-top: 1vh;
  font-weight: 700;
`

const strings = {
  topCardText: "Top Card Text",
  title: "Lorem ipsum dolor",
  nameText: "Name Text",
  boldNameText: "Bold Name Text",
  contentSquareBottomCard: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eget feugiat ipsum."
}

export const Card = () => {
  return(
    <Container>
      <TopSubcontainer>
        <TopText>
          {strings.topCardText}
        </TopText>
        <TitleText>
          {strings.title}
        </TitleText>
        <NamesText>
          <b style={{color:"#525559", marginRight: "1vw"}}>
            {strings.boldNameText}
          </b>
          {strings.nameText}
        </NamesText>
        <NamesText>
          <b style={{color:"#525559", marginRight: "1vw"}}>
            {strings.boldNameText}
          </b>
          {strings.nameText}
        </NamesText>
      </TopSubcontainer>
      <BottomSubcontainer>
        <NamesText>
          {strings.title}
        </NamesText>
        <NamesText>
          <b style={{color:"#525559"}}>
            {strings.contentSquareBottomCard}
          </b>
        </NamesText>
      </BottomSubcontainer>
    </Container>
  )
}
